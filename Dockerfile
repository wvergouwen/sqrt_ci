# syntax = docker/dockerfile:experimental
# ----------------------------------------------------------------
#       Dockerfile to build working Ubuntu image with tue-env
# ----------------------------------------------------------------

 

# Set default base image to Ubuntu 20.04
ARG BASE_IMAGE=ubuntu:20.04
FROM $BASE_IMAGE

 

# Build time arguments
# BRANCH is the target branch if in PULL_REQUEST mode else it is the test branch
ARG CI=false
ARG BRANCH=
ARG PULL_REQUEST=false
ARG COMMIT=
ARG REF_NAME=

 

# Inform scripts that no questions should be asked and set some environment
# variables to prevent warnings and errors
ENV DEBIAN_FRONTEND=noninteractive \
    LANG=C.UTF-8 \
    DOCKER=true \
    USER=avular \
    TERM=xterm-256color

 

# Set default shell to be bash
SHELL ["/bin/bash", "-c"]

 

# Install commands used in our scripts and standard present on a clean ubuntu
# installation and setup a user with sudo priviledges
RUN apt-get update -qq && \
    echo "resolvconf resolvconf/linkify-resolvconf boolean false" | debconf-set-selections && \
    apt-get install -qq --assume-yes --no-install-recommends apt-transport-https apt-utils bash-completion ca-certificates curl dbus debconf-utils dialog git lsb-release iproute2 iputils-ping net-tools openssh-client resolvconf sudo systemd tzdata wget > /dev/null && \
    # Add defined user
    adduser -u 1000 --disabled-password --gecos "" $USER && \
    usermod -aG sudo $USER && \
    usermod -aG adm $USER && \
    usermod -aG dialout $USER && \
    usermod -aG video $USER && \
    usermod -aG audio $USER && \
    echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/"$USER"

 

# Setup the current user and its home directory
USER "$USER"
WORKDIR /home/"$USER"

 

ADD sqrt_simple.cpp ./sqrt_simple.cpp

 

RUN sudo apt-get install -y googletest pkg-config libgtest-dev build-essential
RUN g++ sqrt_simple.cpp $(pkg-config --libs --cflags --static gtest gtest_main)
