#include <iostream>
#include "gtest/gtest.h"

float squareroot(float num) {

    if (num < 0) return -1;
    float temp, sqrt;
    float number;
    number = num;
    sqrt = number / 2;
    temp = 0;

    // It will stop only when temp is the square of our number
    while(sqrt != temp){

        // setting sqrt as temp to save the value for modifications
        temp = sqrt;

        // main logic for square root of any number (Non Negative)
        sqrt = ( number/temp + temp) / 2;
    }

    std::cout << "Square root of '" << number << "' is '" << sqrt << "'" << std::endl;

   return (sqrt);
}

TEST (SquareRootTest, PositiveNos) { 
    EXPECT_EQ (18.0, squareroot (324.0));
    EXPECT_EQ (4.0, squareroot (16.0));
    EXPECT_NEAR (25.4, squareroot (645.16),0.1);
}

TEST (SquareRootTest, ZeroAndNegativeNos) { 
    ASSERT_EQ (0.0, squareroot (0.0));
    ASSERT_EQ (-1, squareroot (-22.0));
}
